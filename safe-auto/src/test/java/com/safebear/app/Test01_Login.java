package com.safebear.app;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * Created by cca_student on 30/08/2017.
 */
public class Test01_Login extends BaseTest {

    @Test
    public void testLogin(){
        assertTrue(true);
//        Step 1 Confirm we're on the welcome page
        assertTrue(welcomePage.checkCorrectPage());
//        Step 2 Click on Login Link and login page loads
        assertTrue(welcomePage.clickOnLogin(this.loginPage));
//        Step 3 Login with valid credentials
        assertTrue(loginPage.login(this.userPage,"testuser","testing"));
    }
}
